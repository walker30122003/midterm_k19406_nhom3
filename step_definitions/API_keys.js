const { I } = inject()
const APIFunction = require('../page/api_keys/index.js')
const APILocator = require('../page/api_keys/locator.js')
const Myfunctions = require('../page/common/functions');
const MyVariable = require('../page/common/variable.js');

Given('I create an API key', () => {
APIFunction.createAPIKey(MyVariable.randomName)
});

Given('I update an API key', () => {
    APIFunction.editAPIKey(MyVariable.randomName)
});
Given('I delete an API key', () => {
    APIFunction.deleteAPIKey()
});