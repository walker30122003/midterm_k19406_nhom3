const { I } = inject()
const timeout = require('../common/timeout')
const settingPageLocator = require('./locator')
const customMethod = require("../../utils/customMethod")
const createNewCompanyLocator = require('../createNewCompany/locator')

module.exports = {
    viewCreationPage() {
        customMethod.clickElement(settingPageLocator.profileNameLabel)
        customMethod.clickElement(settingPageLocator.changeCompanyButton)
        customMethod.clickElement(settingPageLocator.addNewButton)
        I.waitForElement(createNewCompanyLocator.titleText, timeout.waitForElement)
    }
}   