module.exports = {
    
    titleText: "//h4[text()[contains(.,'Tạo tài khoản doanh nghiệp')]]",
    websiteField: "//input[@formcontrolname='website']",
    companyNameField: "//input[@formcontrolname='name']",
    createCompanyButton: "//button//span[text()[contains(.,'Tạo doanh nghiệp')]]",
    optionsRadio: "//div[@class='segment-feature']//mat-radio-button",
    createButton: "//button//span[text()[contains(.,'Tạo một doanh nghiệp')]]",
    companyNameOutPut:"//div[@class='ng-star-inserted']//span[@class='mat-tooltip-trigger text-sm pl-3 ng-star-inserted']"
}