const { I } = inject()
const timeout = require('../common/timeout')
const apiPageLocator = require('./locator')
const customMethod = require("../../utils/customMethod")



module.exports = {
    createAPIKey(key) {
        //customMethod.clickElement(apiPageLocator.buttonMenu)
        customMethod.clickElement(apiPageLocator.buttonThietLap)
        customMethod.clickElement(apiPageLocator.buttonAPI)
        customMethod.clickElement(apiPageLocator.addNewAPIButton)
        customMethod.fieldValue(apiPageLocator.apiNameInput, key)
        customMethod.clickElement(apiPageLocator.createAPIButton)
        customMethod.clickElement(apiPageLocator.doneAPIButton)
    },
    editAPIKey(key) {
        //customMethod.clickElement(apiPageLocator.buttonMenu)
        customMethod.clickElement(apiPageLocator.buttonThietLap)
        customMethod.clickElement(apiPageLocator.buttonAPI)
        customMethod.clickElement(apiPageLocator.editAPIButton)
        customMethod.fieldValue(apiPageLocator.apiNameInput, key)
        customMethod.clickElement(apiPageLocator.updateAPIButton)
        customMethod.clickElement(apiPageLocator.doneAPIButton)
    }
    ,
    deleteAPIKey() {
       // customMethod.clickElement(apiPageLocator.buttonMenu)
        //customMethod.clickElement(apiPageLocator.buttonThietLap)
       // customMethod.clickElement(apiPageLocator.buttonAPI)
        customMethod.clickElement(apiPageLocator.deleteAPIButton)
        customMethod.clickElement(apiPageLocator.agreeButton)
    }
}   